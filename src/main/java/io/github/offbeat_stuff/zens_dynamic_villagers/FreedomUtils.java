package io.github.offbeat_stuff.zens_dynamic_villagers;

import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.brain.MemoryModuleType;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.collection.PackedIntegerArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

public class FreedomUtils {

  private static int MAX_DEPTH = 32;
  private static int DEPTH = 16;
  private static int IARRAY_SIZE = DEPTH * 2;
  private static int getIndex(int x, int y, int z) {
    var ix = x + DEPTH;
    var iy = y + DEPTH;
    var iz = z + DEPTH;
    if (ix < 0 || iy < 0 || iz < 0) {
      return -1;
    }
    if (ix >= IARRAY_SIZE || iy >= IARRAY_SIZE || iz >= IARRAY_SIZE) {
      return -1;
    }
    return iz * IARRAY_SIZE * IARRAY_SIZE + iy * IARRAY_SIZE + ix;
  }

  private static int getIndex(BlockPos pos) {
    return getIndex(pos.getX(), pos.getY(), pos.getZ());
  }

  private static int getIndex(BlockPos villager, BlockPos node) {
    return getIndex(node.subtract(villager));
  }

  private static final Direction[] directions = new Direction[] {
      Direction.EAST, Direction.WEST, Direction.NORTH, Direction.SOUTH};

  private static void markSuccessorsInDirection(ServerWorld world,
                                                VillagerEntity villager,
                                                BlockPos prev, Direction dir,
                                                PackedIntegerArray iarray,
                                                int nextDepth) {
    var next =
        PathfindUtils.getNextNode(world, villager, prev, prev.offset(dir));
    if (next == null) {
      return;
    }
    if (!PathfindUtils.canPathfindThrough(world, next)) {
      return;
    }
    var idx = getIndex(villager.getBlockPos(), next);
    if (idx < 0) {
      return;
    }
    if (iarray.get(idx) != 0) {
      return;
    }
    iarray.set(idx, 1);
    markSuccessors(world, villager, next, iarray, nextDepth);
  }

  private static void markSuccessors(ServerWorld world, VillagerEntity villager,
                                     BlockPos prev, PackedIntegerArray iarray,
                                     int depth) {
    if (depth >= MAX_DEPTH) {
      return;
    }
    for (Direction dir : directions) {
      markSuccessorsInDirection(world, villager, prev, dir, iarray, depth + 1);
    }
  }

  private static int countOnes(PackedIntegerArray iarray) {
    AtomicInteger r = new AtomicInteger();
    iarray.forEach(r::getAndAdd);
    return r.get();
  }

  private static PackedIntegerArray getFreeBlocks(ServerWorld world,
                                                  VillagerEntity villager) {
    var pos = villager.getBlockPos();
    var iarray =
        new PackedIntegerArray(1, IARRAY_SIZE * IARRAY_SIZE * IARRAY_SIZE);
    iarray.set(getIndex(BlockPos.ORIGIN), 1);
    markSuccessors(world, villager, pos, iarray, 0);
    return iarray;
  }

  private static boolean bedCheck(ServerWorld world, VillagerEntity villager) {
    var mem = villager.getBrain().getOptionalMemory(MemoryModuleType.HOME);
    if (mem == null || mem.isEmpty()) {
      return false;
    }
    var pos = mem.get();
    if (world.getRegistryKey() != pos.getDimension()) {
      return false;
    }
    return world.getBlockState(pos.getPos()).isIn(BlockTags.BEDS);
  }

  public static boolean isFree(ServerWorld world, VillagerEntity villager) {
    var iarray = getFreeBlocks(world, villager);
    // VillagerMod.LOGGER.info("{}", countOnes(iarray));
    if (countOnes(iarray) < 16) {
      return false;
    }
    var otherVillagers =
        world.getOtherEntities(villager, villager.getBoundingBox().expand(10),
                               f -> f.getType().equals(EntityType.VILLAGER));
    var reachable = otherVillagers.stream()
                        .filter(f
                                -> iarray.get(getIndex(villager.getBlockPos(),
                                                       f.getBlockPos())) == 1)
                        .count();
    var areEnoughVillagersReachable = reachable >= (otherVillagers.size() / 2);
    return areEnoughVillagersReachable && bedCheck(world, villager);
  }
}
