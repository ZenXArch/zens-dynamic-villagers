package io.github.offbeat_stuff.zens_dynamic_villagers.mixin;

import io.github.offbeat_stuff.zens_dynamic_villagers.FreedomUtils;
import io.github.offbeat_stuff.zens_dynamic_villagers.VillagerTimer;
import net.minecraft.entity.passive.MerchantEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.village.TradeOfferList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MerchantEntity.class)
public class MerchantEntityMixin {
  @Inject(method = "getOffers", at = @At("RETURN"), cancellable = true)
  private void modifyOffers(CallbackInfoReturnable<TradeOfferList> cir) {
    var merchant = (MerchantEntity)(Object)this;
    if (!(merchant instanceof VillagerEntity villager)) {
      return;
    }
    if (!(villager.getWorld() instanceof ServerWorld world)) {
      return;
    }

    var offers = cir.getReturnValue();

    if (!FreedomUtils.isFree(world, villager)) {
      cir.setReturnValue(new TradeOfferList());
      return;
    }

    ((VillagerTimer)villager).zens_dynamic_villagers$resetFunc(offers);
  }
}
