package io.github.offbeat_stuff.zens_dynamic_villagers.mixin;

import io.github.offbeat_stuff.zens_dynamic_villagers.VillagerMod;
import io.github.offbeat_stuff.zens_dynamic_villagers.VillagerTimer;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.village.TradeOfferList;
import net.minecraft.village.TradeOffers;
import net.minecraft.village.VillagerData;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(VillagerEntity.class)
public abstract class VillagerEntityMixin implements VillagerTimer {

  @Unique private Integer timer;

  @Override
  public void zens_dynamic_villagers$resetFunc(TradeOfferList offers) {
    if (this.timer > 0) {
      return;
    }

    if (offers.isEmpty()) {
      return;
    }

    var villager = (VillagerEntity)(Object)this;

    if (!villager.hasCustomer() && !this.levelingUp) {
      offers.remove(offers.size() - 1);
      offers.remove(offers.size() - 1);
      this.fillRecipes();
      this.timer = VillagerMod.nextTimer();
    }

    this.timer = VillagerMod.nextTimer();
  }

  @Shadow protected abstract void fillRecipes();

  @Shadow private boolean levelingUp;

  @Inject(method = "mobTick", at = @At("HEAD"))
  private void modifyOffers(CallbackInfo info) {
    var villager = (VillagerEntity)(Object)this;
    // var merchant = (MerchantEntityAccessor)villager;
    if (!(villager.getWorld() instanceof ServerWorld)) {
      return;
    }

    if (this.timer == null) {
      this.timer = VillagerMod.nextTimer();
    }

    if (this.timer > 0) {
      this.timer--;
    }
  }

  @ModifyConstant(method = "fillRecipes", constant = @Constant(intValue = 2))
  private int modifyTradesPerLevel(int before) {
    return 3;
  }

  @Inject(method = "readCustomDataFromNbt", at = @At("TAIL"))
  private void nbtIn(NbtCompound nbt, CallbackInfo info) {
    if (nbt.contains("resetTimer", NbtElement.INT_TYPE)) {
      this.timer = nbt.getInt("resetTimer");
    }
  }

  @Inject(method = "writeCustomDataToNbt", at = @At("TAIL"))
  private void nbtOut(NbtCompound nbt, CallbackInfo info) {
    if (this.timer != null) {
      nbt.putInt("resetTimer", this.timer);
    }
  }
}