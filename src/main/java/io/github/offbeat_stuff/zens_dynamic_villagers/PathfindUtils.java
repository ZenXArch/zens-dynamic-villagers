package io.github.offbeat_stuff.zens_dynamic_villagers;

import java.util.EnumSet;
import net.minecraft.block.AbstractRailBlock;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.pathing.LandPathNodeMaker;
import net.minecraft.entity.ai.pathing.PathNodeType;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.BlockView;

public class PathfindUtils {
  private static int entityBlockXSize =
      MathHelper.floor(EntityType.VILLAGER.getDimensions().width + 1.0F);
  private static int entityBlockYSize =
      MathHelper.floor(EntityType.VILLAGER.getDimensions().height + 1.0F);
  private static int entityBlockZSize =
      MathHelper.floor(EntityType.VILLAGER.getDimensions().width + 1.0F);

  private static PathNodeType getNodeType(BlockView world, BlockPos pos) {
    return LandPathNodeMaker.getLandNodeType(world, pos.mutableCopy());
  }

  private static PathNodeType adjustNodeType(BlockView world, BlockPos pos,
                                             PathNodeType type) {
    if (type == PathNodeType.DOOR_WOOD_CLOSED) {
      type = PathNodeType.WALKABLE_DOOR;
    }

    if (type == PathNodeType.RAIL &&
        !(world.getBlockState(pos).getBlock() instanceof AbstractRailBlock) &&
        !(world.getBlockState(pos.down()).getBlock() instanceof
          AbstractRailBlock)) {
      type = PathNodeType.UNPASSABLE_RAIL;
    }

    return type;
  }

  private static PathNodeType getNodeType(BlockView world, int x, int y, int z,
                                          MobEntity mob) {
    EnumSet<PathNodeType> enumSet = EnumSet.noneOf(PathNodeType.class);
    PathNodeType pathNodeType = PathNodeType.BLOCKED;
    pathNodeType = findNearbyNodeTypes(world, x, y, z, enumSet, pathNodeType,
                                       mob.getBlockPos());
    if (enumSet.contains(PathNodeType.FENCE)) {
      return PathNodeType.FENCE;
    } else if (enumSet.contains(PathNodeType.UNPASSABLE_RAIL)) {
      return PathNodeType.UNPASSABLE_RAIL;
    } else {
      PathNodeType pathNodeType2 = PathNodeType.BLOCKED;

      for (PathNodeType pathNodeType3 : enumSet) {
        if (mob.getPathfindingPenalty(pathNodeType3) < 0.0F) {
          return pathNodeType3;
        }

        if (mob.getPathfindingPenalty(pathNodeType3) >=
            mob.getPathfindingPenalty(pathNodeType2)) {
          pathNodeType2 = pathNodeType3;
        }
      }

      return pathNodeType == PathNodeType.OPEN &&
              mob.getPathfindingPenalty(pathNodeType2) == 0.0F &&
              entityBlockXSize <= 1
          ? PathNodeType.OPEN
          : pathNodeType2;
    }
  }

  private static PathNodeType
  findNearbyNodeTypes(BlockView world, int x, int y, int z,
                      EnumSet<PathNodeType> nearbyTypes, PathNodeType type,
                      BlockPos pos) {
    for (int i = 0; i < entityBlockXSize; ++i) {
      for (int j = 0; j < entityBlockYSize; ++j) {
        for (int k = 0; k < entityBlockZSize; ++k) {
          int l = i + x;
          int m = j + y;
          int n = k + z;
          PathNodeType pathNodeType =
              getNodeType(world, new BlockPos.Mutable(l, m, n));
          pathNodeType = adjustNodeType(world, pos, pathNodeType);
          if (i == 0 && j == 0 && k == 0) {
            type = pathNodeType;
          }

          nearbyTypes.add(pathNodeType);
        }
      }
    }

    return type;
  }

  public static boolean canPathfindThrough(ServerWorld world, BlockPos pos) {
    if (!world.getFluidState(pos).getFluid().equals(Fluids.EMPTY)) {
      return false;
    }
    var pathNodeType = getNodeType(world, pos);
    var pathNodeType2 = getNodeType(world, pos.up());
    return !pathNodeType.equals(PathNodeType.OPEN) &&
        pathNodeType.getDefaultPenalty() >= 0.0 &&
        pathNodeType2.getDefaultPenalty() >= 0.0;
  }

  public static BlockPos getNextNode(ServerWorld world, VillagerEntity villager,
                                     BlockPos prev, BlockPos pos) {
    var pathNodeType =
        getNodeType(world, pos.getX(), pos.getY(), pos.getZ(), villager);
    var f = villager.getPathfindingPenalty(pathNodeType);
    if (f < 0.0) {
      if (getNodeType(world, prev.up(2)).getDefaultPenalty() >= 0.0) {
        return pos.up(1);
      }
      return null;
    }
    var bpos = pos.mutableCopy();
    if (pathNodeType == PathNodeType.OPEN) {
      int i = 0;

      while (pathNodeType == PathNodeType.OPEN) {
        bpos.move(Direction.DOWN);
        if (bpos.getY() < world.getBottomY()) {
          return null;
        }

        if (i++ >= villager.getSafeFallDistance()) {
          return null;
        }

        pathNodeType =
            getNodeType(world, pos.getX(), bpos.getY(), pos.getZ(), villager);
        f = villager.getPathfindingPenalty(pathNodeType);
        if (pathNodeType != PathNodeType.OPEN && f >= 0.0F) {
          break;
        }

        if (f < 0.0F) {
          return null;
        }
      }
    }

    return bpos;
  }
}
